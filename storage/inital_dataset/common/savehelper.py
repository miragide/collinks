import json, os

def save(info, filename):
  # Récupération du répertoire racine du repo
  buildFolder = os.path.join(os.path.dirname(__file__), '..', '..', '..', 'build')
  
  if not os.path.exists(buildFolder):
        os.makedirs(buildFolder)

  statsFilePath = os.path.abspath(
      os.path.join(buildFolder, '{}.json'.format(filename)))
  with open(statsFilePath, "w") as file_write:
    json.dump([ob.toJSON() for ob in info], file_write, indent=4)
  return statsFilePath