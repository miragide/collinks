# coding: utf-8

# toutes les chaines sont en unicode (même les docstrings)
from __future__ import unicode_literals

from rocketchat_API.rocketchat import RocketChat
import json
import os, sys, getopt
from datetime import datetime
from common.rocketchathelper import getAllChannels, getAllMessages, Connection
from common.savehelper import save
from common.exceptions import NoURL
import re 
import logging
from logging.handlers import RotatingFileHandler
from time import sleep
from document import Document, Origin

# Main configuration

script_name="generate_chat_dataset"

# Logging configuration:
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')

ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
ch.setFormatter(formatter)
logger.addHandler(ch)

# 'append' file, 1 backup, size 1mo
file_handler = logging.handlers.RotatingFileHandler(script_name+'.log', 'a', 1000000, 1)
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

#Add the detail logs
file_handler_details = logging.handlers.RotatingFileHandler(script_name+'_details.log', 'a', 1000000, 1)
file_handler_details.setLevel(logging.DEBUG)
file_handler_details.setFormatter(formatter)
logger.addHandler(file_handler_details)

def main(argv):
    saveFile = False
    try:
        opts, _ = getopt.getopt(argv,"hs",["save"])
    except getopt.GetoptError:
        logging.info(script_name+".py [options]")        
        logging.info("-s or --save : To save the result in a json file")
        sys.exit(2)

    for opt, _ in opts:
        if opt == '-h':
            logging.info(script_name+".py [options]")        
            logging.info("-s or --save : To save the result in a json file")
            sys.exit()
        elif opt in ("-s", "--save"):
            logging.info("Save Option enabled")   
            saveFile = True
    try:
        rocket = Connection()
    except Exception as error:
        logging.error('Impossibe to connect in the rocketchat:')
        logging.error(error)
        sys.exit(2)
        
    URLs_tab = []
    ### Here, store the regex for url filtering. feature to be implemented
    bot_list=['crabot']
    logging.debug('List of bot in the chat: '+  str(bot_list))
    
    API_rate=0
    
    for channel in getAllChannels(rocket):
    #####
    ##### Only to limt the time to parse. By limiting on crabot channel
    #####
        if channel['name'] == 'crabot' : 
            logging.info("currenty " + str(len(URLs_tab)) +" Url in the temp list")
            logging.info( "Start parsing channel: " + channel['name'] + '( '+str(channel["msgs"])+' messages)' )
            messages = getAllMessages(rocket, channel['_id'], count= channel["msgs"])
            logging.debug('Number of mesages in channel (name: '+channel["name"]+' / _id:'+channel['_id']+')')
            if messages is not None:
                for message in messages:
                    try:
                        filter_message(message,channel["name"],URLs_tab,bot_list)
                    except NoURL:
                        pass
            else:
                logging.debug('Message list return NULL for chanel '+channel["name"]+' / _id:'+channel['_id'])
            API_rate +=1
            if API_rate >= 8 : 
                logging.info( "Sleep due to API rate limiter: 90sec ")
                sleep(90)
                API_rate=0
            
    logging.info([ob.toJSON() for ob in URLs_tab])    
    if saveFile:
        try:
            file_name_path = save(URLs_tab, "result")
            logging.info('File created : '+file_name_path)
        except Exception as error:
            logging.error('Impossibe to save the json result file')
            logging.error(error)   
        
    
def filter_message(message, channel,URLs_tab,bot_list):
    if 't' in message:
        raise NoURL()
    if message['u']['username'] in bot_list:
        raise NoURL()
    
    #Filter on "idée" / pad/ etc must be added
    if 'urls' not in message:
        raise NoURL()

    for URL in message['urls']:
        filteredURL_tab = list(filter(lambda doc : doc.url == URL['url'], URLs_tab))
        URLs_aready_existing = len(filteredURL_tab) > 0
        for URLs_existing in filteredURL_tab:
            ### Fix date format to be done
            origin = Origin("RocketChat", str(os.environ['ROCKETCHAT_SERVER'])+"/channel/"+ str(channel), {
                            "user":str(message['u']['username']),
                            "message_date":message['ts'],
                            "channel":str(channel),
                        })
            URLs_existing.addOrigin(origin)
            break
        if not URLs_aready_existing :
            ### Fix date format to be done
            doc = Document(URL['url'])
            origin = Origin("RocketChat", str(os.environ['ROCKETCHAT_SERVER'])+"/channel/"+ str(channel), {
                        "user":str(message['u']['username']),
                        "message_date":message['ts'],
                        "channel":str(channel),
                    })
            doc.addOrigin(origin)
            URLs_tab.append(doc)
        
if __name__ == "__main__":
    logging.info('Script start')
    logging.debug('Args used : ' + str(sys.argv[1:]))
    main(sys.argv[1:])
    logging.info('Script end')