> [<- back](/README.md)

Collinks - web and API
==========================

Local dev env
---------------

Just run 

    docker-compose up collinks-web

at the root of the repo, and check the following section about the http proxy.

### About dinghy-proxy

This piece of dinghy (a docker setup for mac osX) is pretty convenient for having normal hostname on apps (works fine with my linux). It's based on `nginx-proxy`. I combine it with dnsmasq for local resolution of `*.docker` to localhost:

    cat /etc/dnsmasq.d/docker
    address=/docker/127.0.0.1
    listen-address=127.0.0.1,0.0.0.0

https://github.com/codekitchen/dinghy-http-proxy

In our docker-compose, you just have to specify for example:

    environment:
      VIRTUAL_HOST: collinks-web.docker
      VIRTUAL_PORT: 3000

And then you don't need to expose the port:

    # ports:
    #  - "3000:3000"

So now you will reach the web app with http://collinks-web.docker instead of the exposed http://localhost:3000

If you prefer not to use dinghy-proxy, you will need to uncomment the `ports:` sections in `docker-compose.yml`.

To use dinghy-proxy you can run it like this :

    docker run -d --restart=always \
      -v /var/run/docker.sock:/tmp/docker.sock:ro \
      -p 80:80 -p 19322:19322/udp \
      -e CONTAINER_NAME=http-proxy \
      --name http-proxy \
      codekitchen/dinghy-http-proxy

Alternatively to using dnsmasq you can add this to your /etc/hosts file :

    127.0.0.1 collinks-web.docker

The drawback of using the hosts file is that all endpoints have to be added to the file. With dnsmasq a single wildcard *.docker configuration can be used

## Used Techno
* [Node](https://nodejs.org/)
* [Expres](https://expressjs.com/)
* [i18next](https://www.i18next.com/)
* [mongoose](https://mongoosejs.com/)
* [pug](https://pugjs.org/)
* [swagger](https://swagger.io/)
