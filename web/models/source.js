var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var sourceSchema = new Schema({
  name:    String,
  contact: String,
  public: { type: Boolean, default: true },
  keys: [ String ],
  date: { type: Date, default: Date.now },
  hidden: Boolean,
  meta: {
    links: Number,
    users: Number
  }
});

const Source = mongoose.model('Source', sourceSchema);

module.exports = Source;