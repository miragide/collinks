var express = require('express');
var router = express.Router();

/**
 * To document function you should use : https://github.com/Surnet/swagger-jsdoc/blob/master/docs/GETTING-STARTED.md
 */

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send({ result: 'ok'});
});

/**
 * @swagger
 * /addurl?url={url}:
 *  post:
 *    description: Add a new url
 *    produces:
 *      - application/json
 *    parameters:
 *      - name: url
 *        description: url of website
 *        in: url
 *        required: true
 *        type: string
 */
router.post('/addurl', function(req, res, next) {
  res.send({ result: 'Not Implemented'})
});

module.exports = router;
