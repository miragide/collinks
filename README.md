Collinks
=========

A tool for collecting and curating links gathered from various sources.

1 - Summary
------------

### 1.1 - What is that name

Collinks stands for a contraction of `Collective Links`, while at the same time it could be read as `Collecting Links`. This is also kind of a private joke to mock the confusion that the industry is making between [collective intelligence and collecting intelligence][collint].

### 1.2 - History

During 4 years (2013-2017) I was publishing the [Greenruby][greenruby] newsletter every week, curating news about ruby and webdev. At the time I had the idea to automate the collection of links in some way. The idea was to be able to input various sources (newsletters, twitter feeds, rss sources, etc.) into a big depository from which I could pick and chose what I wanted to publish in my newsletter. While it never came to fruition, I still kept a whole lot of thinking about how it could be done and what features it could have.

Now in 2019, after 2 years homesteading the [crapauds fous][cf] community platform (all in french), I feel the same need again. In our rocketchat we share a whole lot of links to valuable sources related to the concerns of the community. We create pads to gather them sometimes, or we create wiki pages, in a very manual and erratic way. It feels that we could extract the urls shared on the chat, puts them in some central place and possibly qualify them, cache their content, and some other features.

So finaly, and because there are other people in the Crapaud Fou community that would like to contribute on such a project, it comes to life. Here is how Collinks is born. But it failed to take off.

9 month later, in the discord of the Canard Refractaire, an independant media, the same need appeared for curation of a huge volume of links. So the Collinks project got back to life, mostly involving 12b and mose that were part of the initial Crapaud fou idea and now use that new context as a playground.

> author: mose

### 1.3 - Purpose

Collinks is a platform for gathering and curating links to web pages and web media, in a collective way. You may remember [dmoz][dmoz] or [yahoo directory][ydir] from before the golden age of search engines. By building such depository of links, we reduce the systematic need for using a search engine. It's also a way to consolidate a collective ontology for a community, as well as tools for handling a shared base of bookmarks, usable in browsers or by chatbots.

2 - Technical specifications
----------------------------

### 2.1 - Architecture

Collinks is ultimately composed by:

- [various extraction scripts](extract/), to get the data from various sources (rocketchat and discord primarily)
- [a storage system](storage/), holding urls information and caching of pages
- [a crawler agent](crawler/) (or a collection of them), gathering data on urls fed into the storage, to augment data about it and cache pages
- [a web UI](web/), for user browsing, ranking, manual categorization and administration
- [an API](web/), for chatbots and external products consumption, but also for all what the UI is providing (ranking, categorization, etc)
- [various web browser extensions](extensions/) for including collected urls as an alternative and collective bookmark system

### 2.2 - Development

This project is going to follow a **[Readme Driven Development][rdd] methodology**. It means that all features are first described in the README files before being coded. That way we can have visibility on what is planned, that's a roadmap-oriented readme. Beyond that, we will also keep track of advancement on each feature, not on a kanban board or some trello board, but in the readme itself. Each feature and gimmick description includes who's working on it and what's the degree of advancement, so that we don't collide with concurrent proposals for each features.

This approach is particularly interesting in our case, as Collinks is not just one app, but a collection of tools that work together in an ecosystem. That way, by specifying how things will behave beforehand, we can work on separate parts before their dependencies are yet implemented.

Note that Gitlab provides a [wide range of features](md) that can be used in markdown pages (including graphs building).

### 2.3 - Repository

The **single-repository** approach is preferred for this project, for consistency sake, at least at the start of the project. Each codebase will be contained in a subdir of the main repo https://gitlab.com/crapaud-fou/collinks.

Additions to some readme or to some codebase can follow the **Merge-request** procedure. Status updates, typographic or textual enhaqnmcement, should not need that and the core team of the project should feel free to just push any changes of that nature.

We keep **master** as the production branch, if there are experimental paths chosen on some codebase, a branch should be created, keeping in mind that this could impact master branch by the need of updating the readme there.

### 2.4 - Coding languages

Each tool in the platform being somehow independant, they don't need to use all the same runtime. But given the pool of initial contributors, we should consider sticking to javascript (ES6) and possibly python3. This being said, nothing opposes to have various implementation of same features. Even more, some data collection system could benefit being written in the language of the origin application. So, there is no real restriction here.

Just please, don't bring up java. Pretty please :)

3 - Legal Stuff
----------------

### 3.1 - Copyright holders

Every committer on the codebases present in this repository is indivisibly holder of an equal copyright share. This excludes contributors that only propose changes to the readme. No decision about changing the license could be made without the agreement of the unanimity of the copyright holders. No other decision than changing the license is expected from copyright holders. The list of copyright holders is maintained in the [AUTHORS.txt](AUTHORS.txt) file, and are referred under the name 'Collinks authors'.

### 3.2 - License

The participation on this project involves the agreement with its availability under the terms of the [GNU/GPL version 3 or above][gpl] license. It implies that no third party code should be included (or depended upon) that is not compliant with that license.

---

(c) copyright 2019 [the Collinks authors](AUTHORS.txt) - available under the terms of [GNU General Public License v3.0 or above](LICENSE.txt).

[collint]: https://blog.mose.com/2017/06/16/collective-intelligence-or-collecting-intelligence/
[greenruby]: https://greenruby.mose.com
[cf]: https://crapaud-fou.org
[dmoz]: https://en.wikipedia.org/wiki/DMOZ
[ydir]: https://en.wikipedia.org/wiki/Yahoo!_Directory
[rdd]: https://tom.preston-werner.com/2010/08/23/readme-driven-development
[md]: https://docs.gitlab.com/ee/user/markdown.html
[gpl]: https://choosealicense.com/licenses/gpl-3.0/
